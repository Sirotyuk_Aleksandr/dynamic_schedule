<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.03.19
 * Time: 18:49
 */

namespace app\models;

use yii\db\ActiveRecord;

class Task extends ActiveRecord
{
    /**
     * Определение названия таблицы в базе данных
     *
     * @return string
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * Отправка задачи (начальной и конечной даты) методом POST в алгоритм с помощью curl
     *
     * @param $url
     * @param $message
     * @return bool|string
     */
    public function transferCurl($url, $message){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
        $result = curl_exec($ch);
        curl_close ($ch);
        return $result;
    }
}