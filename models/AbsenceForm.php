<?php

namespace app\models;

use yii\base\Model;

class AbsenceForm extends Model
{
    public $id_user;
    public $status;

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['status'],'string'],
        ];
    }

    /**
     * Отметить отсутствие
     *
     * @return bool - true, если сохранение в БД произошло
     */
    public function noteAbsence(){
        $absence_model = new Absence();
        $absence_model->id_user = $this->id_user;
        $absence_model->status = $this->status;
        $absence_model->save();
    }
}