<?php

namespace app\models;

use yii\db\ActiveRecord;

class Shift extends ActiveRecord
{
    /**
     * Определение названия таблицы в базе данных
     *
     * @return string
     */
    public static function tableName()
    {
        return 'shifts';
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['id_user','date_start_work', 'date_end_work'],'required'],
            [['remainder_hours_month','work_hours_untill_weekend'],'safe'],
        ];
    }

    /**
     * Нахождение последней смены по идентификатору пользователя
     *
     * @param $id
     * @return array|ActiveRecord|null
     */
    public function getLastShiftByUserId($id){
        $query = self::find()->where(['id_user' => $id])->orderBy(['id' => SORT_DESC])->one();
        return $query;
    }

    /**
     *
     * Установление связи "один к одному"
     *
     * Связь: таблица "shifts" - таблица "users". Поле в "shifts": id_user,
     * поле в "users": id
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}