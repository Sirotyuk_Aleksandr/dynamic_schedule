<?php

namespace app\models;

use yii\base\Model;

class GraphForm extends Model
{
    public $date_from;
    public $date_to;

    /**
     * Названия атрибутов
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date_from' => 'С',
            'date_to' => 'По',
        ];
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['date_from','date_to'],'required'],
            [ ['date_from', 'date_to'],'date', 'format'=>'yy-mm-dd'],
        ];
    }

    /**
     * Добавление задачи в таблицу tasks
     *
     * Задача представляет собой JSON объект с периодом времени,
     * за который нужно составлять график.
     *
     * @return bool - true, если сохранение в БД произошло
     */
    public function addTask(){
        $taskArr = array();
        $taskArr['date_from'] = $this->date_from;
        $taskArr['date_to'] = $this->date_to;
        $taskArrJSON = json_encode($taskArr);

        $task = new Task();
        $task->text = $taskArrJSON;
        $task->processed = 0;
        return $task->save();
    }
}