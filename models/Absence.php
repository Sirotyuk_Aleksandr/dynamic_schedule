<?php

namespace app\models;

use yii\db\ActiveRecord;

class Absence extends ActiveRecord
{
    /**
     * Определение названия таблицы в базе данных
     *
     * @return string
     */
    public static function tableName()
    {
        return 'absence';
    }

    /**
     * Отметка об отсутствии
     *
     * @param $id_user
     * @param $date_absence
     * @return bool
     */
    public function noteAbsence($id_user, $date_absence){
        $this->id_user = $id_user;
        $this->date = $date_absence;
        return $this->save();
    }
}