<?php

namespace app\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    /**
     * Определение названия таблицы в базе данных
     *
     * @return string
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * Получение идентификаторов пользователей
     *
     * @return array|ActiveRecord[]
     */
    public function getUserIDs()
    {
        $query = self::find()->select(['id'])->where(['group' => 3])
            ->orWhere(['group' => 9])->orWhere(['group' => 13])->asArray()->all();
        return $query;
    }
}