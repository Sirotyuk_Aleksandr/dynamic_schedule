/* Отметить отсутствие */

$('.note_absence').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var date = $(this).data('date');
    $.ajax({
        url: '/graph/note-absence',
        data: {id: id, date:date},
        type: 'GET',
        success: function (res) {
            el = e.delegateTarget;
            el.parentNode.removeChild(el);
        },
        error: function() {
            alert('Error!');
        }   
    });
});