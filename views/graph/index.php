<?php
$this->title = 'Составление графика';

use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>
<div class="container-fluid">
    <div class="col-md-2 col-md-offset-5">
        <h3 align="center">Составление графика</h3>
        <?php
        $form = ActiveForm::begin(['class' => 'form-horizontal', 'id' => 'drafting_graph']);
        ?>

        <?= $form->field($graphForm, 'date_from')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]); ?>
        <?= $form->field($graphForm, 'date_to')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]); ?>
        <button type="submit" class="btn btn-primary">Составить график</button>
        <?php
        ActiveForm::end();
        ?>
    </div>
</div>

