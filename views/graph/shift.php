<?php
$this->title = 'График';

$amountDays = date("t");
?>
<section class="shifts">
    <div class="container-fluid">
        <h3 align="center">Список смен</h3>
        <div class="shift-block">
            <table width="100%" cellpadding="5">
                <tr>
                    <th>ФИО</th>
                    <?php for($i = 0; $i < $diffDates; $i++):
                        $date_unix = $date_from + 86400*$i;
                        $date = date('d', $date_unix);
                        ?>
                        <th>
                            <?= $date;?>
                        </th>
                    <?php endfor; ?>
                </tr>
                <?php
                    $flagFound = 0;
                    $j = 0;
                    $buffer = array();
                    $bufferDate = array();
                    foreach ($shifts as $shift):
                        if(!in_array($shift['user']['fio'], $buffer)):
                            $buffer[$j] = $shift['user']['fio'];
                            $j++;
                        ?>
                        <tr>
                            <td><?= $shift['user']['fio']; ?></td>
                            <?php for($i = 0; $i < $diffDates; $i++):
                                $date_unix = $date_from + 86400*$i;
                                $date = date('Y-m-d', $date_unix);
                                for($m = 0; $m < count($shiftsCpy); $m++){
                                    if(($buffer[$j-1] == $shiftsCpy[$m]['user']['fio'])
                                        && ($date == date('Y-m-d', strtotime($shiftsCpy[$m]['date_start_work'])))){
                                        $flagFound = 1;
                                        $bufferDate['date_start_work'] = date('H', strtotime($shiftsCpy[$m]['date_start_work']));
                                        $bufferDate['date_end_work'] = date('H', strtotime($shiftsCpy[$m]['date_end_work']));
                                    }
                                }
                                ?>
                                <?php if($flagFound == 1):
                                        $flagFound = 0;
                                ?>
                                    <td><?= $bufferDate['date_start_work'].'-'.$bufferDate['date_end_work'];?></td>
                                    <?php else: ?>
                                    <td><?= 'В' ?></td>
                                <?php endif;?>
                            <?php endfor; ?>
                        </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
            </table>
        </div>
    </div>
</section>




