<?php

$this->title = 'Отсутствие';

use yii\helpers\Url;
?>

<section class="absence">
    <div class="container-fluid">
        <h3 align="center">Отсутствие</h3>
        <a href="<?= Url::to(['/task/index']) ?>" class="btn btn-primary">Начать построение графика</a>
        <a href="<?= Url::to(['/graph/index']) ?>" class="btn btn-danger">Изменить диапазон дат</a>
        <a href="<?= Url::to(['/graph/absence']) ?>" class="btn btn-danger">Удалить отметки об отсутствии</a>
        <div class="absence-block">
            <table width="100%" cellpadding="5">
                <tr>
                    <th>ФИО</th>
                    <?php for($i = 0; $i < $diffDates; $i++):
                            $date_unix = $date_from + 86400*$i;
                            $date = date('d', $date_unix);
                    ?>
                        <th><?= $date;?></th>
                    <?php endfor; ?>
                </tr>
                <?php
                if(!empty($users))
                    foreach ($users as $user):
                        ?>
                        <tr>
                            <td><?=$user['fio'];?></td>
                            <?php for($i = 0; $i < $diffDates; $i++):
                                $date_unix = $date_from + 86400*$i;
                                $date = date('Y-m-d', $date_unix);
                            ?>
                                <td><a class="note_absence" data-id="<?=$user['id'];?>"
                                       data-date="<?=$date;?>" href="<?= Url::to(['/graph/note-absence',
                                        'id' => $user['id']]) ?>">
                                        <i class="fa fa-plus"></i></a></td>
                            <?php endfor; ?>
                        </tr>
                    <?php endforeach; ?>
            </table>
        </div>
    </div>
</section>






