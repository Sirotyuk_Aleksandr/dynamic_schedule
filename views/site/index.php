<?php
$this->title = 'Составление графика';

use yii\helpers\Html;
?>
<div class="container-fluid">
    <div class="main_page">
        <h1 align="center">Приложение для составления графика</h1>
        <?= Html::img('@web/images/main_picture.png', ['alt' => 'main_picture', 'height' => '680px', 'width' => '680px', 'id' => 'main_picture']); ?>
    </div>
</div>

