def hour12(year, month, day, hour):
    hour += 12
    if hour > 23:
        hour %= 23
        day += 1
        if day > 28:
            if month == 2:
                day %= 28
                month += 1
            elif month == 4 or month == 6 or month == 9 or month == 11:
                if day > 30:
                    day %= 30
                    month += 1
            elif day > 31:
                day %= 31
                if month == 12:
                    year += 1
                    month = 1
                else:
                    month += 1
    return year, month, day, hour

def day2(year, month, day):
    day += 2
    if day > 28:
        if month == 2:
            day %= 28
            month += 1
        elif month == 4 or month == 6 or month == 9 or month == 11:
            if day > 30:
                day %= 30
                month += 1
        elif day > 31:
            day %= 31
            if month == 12:
                year += 1
                month = 1
            else:
                month += 1
    return year, month, day

#
# date_rest_min = datetime.datetime(  int(list_date[0]),
#                                                 int(list_date[1]),
#                                                 int(list_date[2]) + (int(list_hour[0]) + 12) // 23,
#                                                 (int(list_hour[0]) + 12) % 23) # Год, месяц, день, час минимального отдыха