import pandas as pd
import datetime
import requests

import activity
import date

# TODO: Запрос начальной и конечной даты
# TODO: Получение данных с модели
# TODO:

data_start = "2019-01-03"
# data_end = "2018-01-31"
a = data_start.split('-')
aa = datetime.datetime(int(2018),int(12),int(31))

# TODO: Запрос данных об операторах
# TODO: обновление hour_month = 200 в первый день месяца
# TODO: дабавить значения по умолчанию

operator0 = {'id': 0, 'remainder_hours_month': 200, 'date_work_end': aa, 'work_hours_untill_weekend': 0, 'first_day': True}
operator1 = {'id': 1, 'remainder_hours_month': 200, 'date_work_end': aa, 'work_hours_untill_weekend': 0, 'first_day': True}
operator2 = {'id': 2, 'remainder_hours_month': 200, 'date_work_end': aa, 'work_hours_untill_weekend': 0, 'first_day': True}
operator3 = {'id': 3, 'remainder_hours_month': 200, 'date_work_end': aa, 'work_hours_untill_weekend': 0, 'first_day': True}
operator4 = {'id': 4, 'remainder_hours_month': 200, 'date_work_end': aa, 'work_hours_untill_weekend': 0, 'first_day': True}
operator5 = {'id': 5, 'remainder_hours_month': 200, 'date_work_end': aa, 'work_hours_untill_weekend': 0, 'first_day': True}
operator6 = {'id': 6, 'remainder_hours_month': 200, 'date_work_end': aa, 'work_hours_untill_weekend': 0, 'first_day': True}

# first_day = True - Человек не работал первого числа

list_work = pd.read_csv('../data/schedule_201901_for_load.csv')
list_operators = (operator0, operator1, operator2, operator3, operator4, operator5, operator6) # костыль
graph = []

# Цикл по данным из модели
# print('Цикл по данным из модели')
for i in range(3):  # list_work.shape[0]):
    work = list_work.loc[i]  # Считываем по строчке из модели смен
    count = int(work[3])  # Кол-во предсказанных смен
    date_graph_start = datetime.datetime(int(work[6]),
                                         int(work[4]),
                                         int(work[5]),
                                         int(work[1]))  # Год, месяц, дата, час - начала смены
    count_hour = work[7]  # Кол-во часов в смене
    end = int(work[2])
    if end == 24:
        end = 0
    date_graph_end = datetime.datetime(int(work[6]),
                                       int(work[4]),
                                       int(work[5]),
                                       end)  # Год, месяц, дата, час - конца смены
    # printwork(work)
    # print('Цикл по кол-ву смен')
    # Цикл по кол-ву смен
    for i in range(count):
        num = 0  # Индекс оператора
        choice = True
        # print('Цикл по операторам')
        # Цикл по операторам
        while choice:
            people = list_operators[num]  # Данные i-ого оператора
            id = people['id']
            hour_month = people['remainder_hours_month']  # Кол-во оставшихся часов в месяц
            date_work_end = people['date_work_end']
            # Год, месяц, дата, час конца последней отработанной смены оператора
            list_date_time = str(date_work_end).split(' ')  #
            list_date = list_date_time[0].split('-')
            list_hour = list_date_time[1].split(':')
            work_hour = people['work_hours_untill_weekend']  # Сумма отработанных смен
            # print(list_date)
            if activity.Activ(id, date_graph_start):
                # if int(work[5]) == 1 and first_day: #первый день месяца
                # 	people['hour_month'] = 200

                # num += 1
                # print(date_graph_start) # Год, месяц, дата, час - начала смены
                # print('Проверка активности и отработанное время в месяц, Проверка минимального отдыха после смены')
                # Проверка активности и отработанное время в месяц
                # Проверка минимального отдыха после смены
                rest_min = date.hour12(int(list_date[0]),
                                       int(list_date[1]),
                                       int(list_date[2]),
                                       int(list_hour[0]))  # Функция +12 часов
                date_rest_min = datetime.datetime(rest_min[0],
                                                  rest_min[1],
                                                  rest_min[2],
                                                  rest_min[3])  # Год, месяц, день, час минимального отдыха
                rest = date.day2(int(list_date[0]),
                                 int(list_date[1]),
                                 int(list_date[2]))  # Функция +2 дня
                date_rest = datetime.datetime(rest[0],
                                              rest[1],
                                              rest[2],
                                              int(list_hour[0]))  # Год, месяц, день конца выходных
                print(id, hour_month, count_hour)
                print(date_rest_min, date_graph_start)
                if hour_month >= count_hour and date_rest_min <= date_graph_start:
                    # { and
                    # Проверка отработанных смен
                    print('Проверка отработанных смен')
                    # print(date_rest_min)
                    # print(date_graph_start)
                    print(work[1], '-', work[2])
                    if work_hour <= 30:  #
                        # date_work_end = end
                        # hour_month -= count_hour
                        # work_hour += count_hour
                        people['date_work_end'] = date_graph_end  # Заменяем время последней отработанной смены
                        people['remainder_hours_month'] -= count_hour  # Вычитаем кол-во оставшихся часов в месяц
                        people['work_hours_untill_weekend'] += count_hour  # Прибавляем кол-во отработанных часов до выходных
                        # first_day = False
                        print(id, 1)
                        # print(work[1], '-', work[2])
                        # print(people['date_work_end'])

                        graph.append({'id_user': people['id'],
                                      'beginning': int(work[1]),
                                      'end': end,
                                      'day': int(work[5]),
                                      'month': int(work[4]),
                                      'year': int(work[6]),
                                      'graph': None})
                        print(graph)
                        break
                    # work_hour = 0
                    # date_work_end = end
                    # Проверка выходных
                    elif date_rest >= date_graph_start:  # Последняя отработанная смена + 2 дня >= Начало смены
                        # hour_month -= count_hour
                        people['work_hours_untill_weekend'] = count_hour
                        people['date_work_end'] = date_graph_end
                        people['remainder_hours_month'] -= count_hour
                        # first_day = False
                        print(id, 2)
                        graph.append({'id_user': people['id'],
                                      'beginning': int(work[1]),
                                      'end': end,
                                      'day': int(work[5]),
                                      'month': int(work[4]),
                                      'year': int(work[6]),
                                      'graph': None})
                    # print(work[1], '-', work[2])
                    # print(people['date_work_end'])
                    break
            num += 1

res = requests.post("http://127.0.0.1/users", json=graph)

print(res.text)
