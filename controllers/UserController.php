<?php


namespace app\controllers;

use app\models\Shift;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\rest\Controller;

class UserController extends Controller
{
    /**
     * REST API (получение данных о пользователях)
     *
     * @return string
     */
    public function actionIndex()
    {
        $user_model = new User();
        $shift_model = new Shift();

        $users = $user_model->getUserIDs();
            for($i = 0; $i < count($users); $i++){
                $shift = $shift_model->getLastShiftByUserId($users[$i]['id']);
                if($shift){
                    $users[$i]['date_start_work'] = $shift['date_start_work'];
                    $users[$i]['date_end_work'] = $shift['date_end_work'];
                    $users[$i]['remainder_hours_month'] = $shift['remainder_hours_month'];
                    $users[$i]['work_hours_untill_weekend'] = $shift['work_hours_untill_weekend'];
                }
            }
        return $users;
    }

    /**
     * @return array
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
        ];
    }
}