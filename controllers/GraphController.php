<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.03.19
 * Time: 10:53
 */

namespace app\controllers;

use app\models\Absence;
use app\models\Shift;
use app\models\Task;
use app\models\User;
use Yii;
use yii\web\Controller;
use app\models\GraphForm;

class GraphController extends Controller
{
    /**
     * Определение пользователем периода времени для составления графика
     *
     * @return string|\yii\web\Response
     */
    public function actionIndex(){
        $graphForm = new GraphForm();
        if(isset($_POST['GraphForm'])){
            $graphForm->attributes = Yii::$app->request->post('GraphForm');
            if($graphForm->validate() && $graphForm->addTask()){
                return $this->redirect('/graph/absence');
            }
        }
        return $this->render('index', compact('graphForm'));
    }

    /**
     * Отображение таблицы для возможности отметки об отсутствии
     *
     * @return string
     */
    public function actionAbsence(){
        $tasks = Task::find()->orderBy(['id' => SORT_DESC])->one();
        $dates = json_decode($tasks['text'], true);
        $diffDates = (strtotime($dates['date_to']) - strtotime($dates['date_from']))/86400;
        $date_from = strtotime($dates['date_from']);

        $users = User::find()->select(['fio','id'])->where(['group' => 3])
            ->orWhere(['group' => 9])->orWhere(['group' => 13])->asArray()->all();


        return $this->render('absence', compact('diffDates', 'users','date_from'));
    }

    /**
     * Отметка об отсутствии
     *
     * @param $id - идентификатор пользователя
     * @param $date - дата отсутствия
     * @return bool
     */
    public function actionNoteAbsence($id, $date){
        $absence_model = new Absence();
        $absence_model->noteAbsence($id, $date);
        return true;
    }

    /**
     * Отображение рабочего графика
     *
     * @return string
     */
    public function actionShift(){
        $tasks = Task::find()->orderBy(['id' => SORT_DESC])->one();
        $shifts = Shift::find()->asArray()->with('user')->orderBy('id_user')->all();

        $dates = json_decode($tasks['text'], true);
        $diffDates = (strtotime($dates['date_to']) - strtotime($dates['date_from']))/86400;
        $date_from = strtotime($dates['date_from']);

       //ini_set('memory_limit', '1000M');
       //ini_set('max_execution_time', 900);
        $shiftsCpy = $shifts;

        return $this->render('shift', compact(  'shifts', 'diffDates', 'date_from', 'shiftsCpy'));
    }
}