<?php

namespace app\controllers;

use app\models\Absence;
use yii\rest\Controller;

class AbsenceController extends Controller
{
    /**
     * REST API (получение информации об отсутствии операторов)
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionIndex(){
        $absence = Absence::find()->all();
        return $absence;
    }

    /**
     * Удаление отметок об отсутствии
     *
     * @return \yii\web\Response
     */
    public function actionDeleteNotes(){
        Absence::deleteAll();
        return $this->refresh();
    }
}