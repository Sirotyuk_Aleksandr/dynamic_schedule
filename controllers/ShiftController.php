<?php

namespace app\controllers;

use app\models\Absence;
use app\models\Shift;
use app\models\Task;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\rest\Controller;
use yii\web\ServerErrorHttpException;

class ShiftController extends Controller
{
    /**
     * REST API (добавление смены в базу данных)
     *
     * @return bool - true, если сохранение записи в базу данных произошло успешно
     */
    public function actionIndex()
    {
        Shift::deleteAll();
        $request = json_decode(Yii::$app->getRequest()->getRawBody(), true);
        for ($i = 0; $i < count($request); $i++){
            $shiftModel = new Shift();
            $shiftModel->id_user = $request[$i]['id_user'];
            $shiftModel->date_start_work = $request[$i]['date_start_work'];
            $shiftModel->date_end_work = $request[$i]['date_end_work'];
            if(isset($request[$i]['remainder_hours_month'])){
                $shiftModel->remainder_hours_month  = $request[$i]['remainder_hours_month'];
            }
            else{
                $shiftModel->remainder_hours_month = 200;
            }
            if(isset($request[$i]['work_hours_untill_weekend'])){
                $shiftModel->work_hours_untill_weekend 	 = $request[$i]['work_hours_untill_weekend'];
            }
            else{
                $shiftModel->work_hours_untill_weekend = 0;
            }
            $shiftModel->save();
        }
        $task =  Task::find()->orderBy(['id' => SORT_DESC])->one();
        $task->processed = 1;
        $task->save();

        Absence::deleteAll();
        return true;
    }
}