<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.03.19
 * Time: 9:04
 */

namespace app\controllers;

use app\models\Task;
use yii\helpers\Json;
use yii\web\Controller;

class TaskController extends Controller
{
    /**
     * Отправка задач
     *
     * @return
     */
    public function actionIndex()
    {
        $taskModel = new Task();
        $query = Task::find()->select(['id','text'])->where(['processed' => 0])->all();
        $result = array();
        $result = Json::decode($query[count($query)-1]['text']);
        $result['id'] = $query[count($query)-1]['id'];
        $url = '10.80.18.199:8080';
        $resultJSON = json_encode($result);
        $taskModel->transferCurl($url, $resultJSON);
        return $this->redirect('/graph/shift');
    }

}