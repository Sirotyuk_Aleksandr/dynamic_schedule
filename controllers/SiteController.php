<?php

namespace app\controllers;

use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * Отображение главной страницы
     *
     * @return string
     */
    public function actionIndex(){
        return $this->render('index');
    }

}